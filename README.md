# bdenv
Docker infrastructure for Big Data components along with sample projects

Components included:

- Apache Hadoop 2.7.4
- Apache Hive 2.3.2
- Apache Flink 1.8
- Apache HBase 1.2.6
- Apache Spark 2.4.1
- Apache Cassandra 3.11.1
- Apache Zookeeper 3.4.10
- Apache Kafka 2.2.1
- Redis 4.0.2

## usage

run `make help` to see help about docker infrastructure.
